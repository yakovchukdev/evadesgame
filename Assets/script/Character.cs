using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Character : MonoBehaviour
{
    private int Level = 1;
    private float _mana;
    private float _manaRegen;
    [SerializeField] private VariableJoystick variableJoystick;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Next lvl")
        {
            LevelSystem();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        if (other.gameObject.tag == "Slower")
        {
            JoystickPlayerExample.speed /= 2;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Slower")
        {
            JoystickPlayerExample.speed *= 2;
        }
    }
    private void LevelSystem()
    {
        Level++;

    }
    private void PressForTeleport()
    {
       /* Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        rb.AddForce(direction * speed * Time.fixedDeltaTime, ForceMode.VelocityChange);
       transform.position=new Vector3()*/

    }
}
