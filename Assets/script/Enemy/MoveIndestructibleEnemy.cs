using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIndestructibleEnemy : MonoBehaviour
{
    private Rigidbody2D _rb;
    [SerializeField] private float z,x;
    private float helpY=90,y=0;
    private void FixedUpdate()
    {
        transform.Translate(new Vector3(x*Time.deltaTime, 0, z * Time.deltaTime));
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Wall")
        {
            DirectioAndSpeedMovement();
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            DirectioAndSpeedMovement();
        }
        
    }
    private void DirectioAndSpeedMovement()
    {
        transform.Rotate(0,y+ helpY, 0);
    }
}
