using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.script.Enemy
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private List<GameObject> _smallEnemy;
        [SerializeField] private List<GameObject> _mediumEnemy;
        [SerializeField] private List<GameObject> _bigEnemy;
        [SerializeField] private List<GameObject> _BossEnemy;

        private Vector3 _spawnArea;
        private void Start()
        {
            _spawnArea = new Vector3(Random.Range(-30.0f, 30.0f), 2, Random.Range(-5.0f, 5.0f));

            Spawner();
        }
        private void Spawner()
        {

            foreach (var enemy in _smallEnemy)
            {
                Instantiate(enemy, _spawnArea, Quaternion.identity);
                enemy.transform.localScale = new Vector3(0.5F, 0.5F, 0.5F);
            }
            foreach (var enemy in _mediumEnemy)
            {
                Instantiate(enemy, _spawnArea, Quaternion.identity);
                enemy.transform.localScale = new Vector3(1, 1, 1);
            }
            foreach (var enemy in _bigEnemy)
            {
                Instantiate(enemy, _spawnArea, Quaternion.identity);
                enemy.transform.localScale = new Vector3(2, 2, 2);
            }
            foreach (var enemy in _BossEnemy)
            {
                Instantiate(enemy, _spawnArea, Quaternion.identity);
                enemy.transform.localScale = new Vector3(4, 4, 4);
            }

        }
    }
}