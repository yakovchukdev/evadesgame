using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveSlowerEnemy : MonoBehaviour
{


    private Rigidbody2D _rb;
    [SerializeField] private float x, z;
    private float helpZ;
    private byte checker = 5;
    private void Start()
    {
        DirectioAndSpeedMovement();
    }
    private void FixedUpdate()
    {
        transform.Translate(new Vector3(x * Time.deltaTime, 0, z * Time.deltaTime));
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Wall")
        {
            if (z < 0)
                z -= z * 2;
            else
                z = -z;
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            if (x < 0)
                x -= x * 2;
            else
                x = -x;
        }
    }
    private void DirectioAndSpeedMovement()
    {
        x = Random.Range(-5.0f, 5.0f);
        if (x > 0)
        {
            helpZ = checker - x;
            z = Random.Range(1, 3);

            if (z == 1)
                z = -helpZ;
            else if (z == 2)
                z = helpZ;
        }
        else if (x < 0)
        {
            helpZ = checker + x;
            z = Random.Range(1, 3);

            if (z == 1)
                z = -helpZ;
            else if (z == 2)
                z = helpZ;
        }
        else
        {
            z = Random.Range(1, 3);

            if (z == 1)
                z = -10;
            else if (z == 2)
                z = 10;
        }
    }
}